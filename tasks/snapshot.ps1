<#
.SYNOPSIS
    takes a VSS snapshot
.DESCRIPTION
    this takes a vss snapshot on the specified drive.
.PARAMETER Drive
    the drive to take a vss snapshot of
.EXAMPLE
    C:\PS> snapshot.ps1 -Drive C
    take a snapshot of the C drive. 
#>

#requires -version 3
[cmdletbinding()]

param(
    [Parameter(Mandatory=$true)]
    [String] $Drive = 'C'
)
begin {
    $vss = Get-WmiObject -List win32_shadowcopy 
}
process {
    if( $Drive -imatch '^[A-Z]$' ){
        $Drive = "${Drive}:\"
    }
    $vss.create("$Drive", 'ClientAccessible')
}