require 'spec_helper'
require 'date'

describe 'vss::schedule' do
  title = 'C'
  now = DateTime.now.strftime('%H:%M')
  let(:title) { title.to_s }
  let(:params) do
    {
      'start_time' => now,
    }
  end

  schedules = ['daily', 'weekly', 'monthly', 'once']

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      schedules.each do |sched|
        context "with schedule #{sched}" do
          let(:params) do
            super().merge('run_schedule' => sched)
          end

          case sched
          when 'monthly'
            let(:params) do
              super().merge(
                'run_schedule' => sched,
                'on'           => [1],
                'months'       => [1],
              )
            end

            it {
              is_expected.to contain_scheduled_task("ShadowCopy{#{title}}").with_trigger(
                'schedule'         => 'monthly',
                'start_time'       => now,
                'start_date'       => DateTime.now.strftime('%Y-%m-%d'),
                'minutes_interval' => 1,
                'minutes_duration' => 5,
                'on'               => [1],
                'months'           => [1],
              )
            }
          when 'weekly'
            let(:params) do
              super().merge(
                'run_schedule' => sched,
                'day_of_week'  => ['mon'],
              )
            end

            it {
              is_expected.to contain_scheduled_task("ShadowCopy{#{title}}").with_trigger(
                'schedule'         => 'weekly',
                'start_time'       => now,
                'start_date'       => DateTime.now.strftime('%Y-%m-%d'),
                'minutes_interval' => 1,
                'minutes_duration' => 5,
                'every'            => 1,
                'day_of_week'      => ['mon'],
              )
            }
          when 'daily'
            it {
              is_expected.to contain_scheduled_task("ShadowCopy{#{title}}").with_trigger(
                'schedule'         => 'daily',
                'start_time'       => now,
                'start_date'       => DateTime.now.strftime('%Y-%m-%d'),
                'minutes_interval' => 1,
                'minutes_duration' => 5,
                'every'            => 1,
              )
            }
          when 'once'
            it {
              is_expected.to contain_scheduled_task("ShadowCopy{#{title}}").with_trigger(
                'schedule'         => 'once',
                'start_time'       => now,
                'start_date'       => DateTime.now.strftime('%Y-%m-%d'),
                'minutes_interval' => 1,
                'minutes_duration' => 5,
              )
            }
          end
          it { is_expected.to compile }
          it {
            is_expected.to contain_scheduled_task("ShadowCopy{#{title}}").with(
              'ensure'    => 'present',
              'command'   => 'C:\\Windows\\system32\\vssadmin.exe',
              'arguments' => "Create Shadow /AutoRetry=15 /For=#{title}:",
            )
          }
        end
      end
    end
  end
end
