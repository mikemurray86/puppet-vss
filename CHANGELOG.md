# Changelog

All notable changes to this project will be documented in this file.
## Release 0.2.1

**Features**
- add support for explicit file sizes ([issue #3](https://gitlab.com/mikemurray86/puppet-vss/issues/3))

**Bugfixes**
- fix for failure to apply storage settings when no storage was set before ([issue #1](https://gitlab.com/mikemurray86/puppet-vss/issues/1))
- Able to create shadow storage when no storage was set before ([issue #2](https://gitlab.com/mikemurray86/puppet-vss/issues/2))
**Removed**
- support for windows 10

## Release 0.1.0

**Features**

**Bugfixes**

**Known Issues**
