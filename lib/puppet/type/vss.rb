Puppet::Type.newtype(:vss) do
  desc 'A type to describe how to interact with volume shadow services'
  ensurable
  newparam(:drive, namevar: :true) do
    desc 'the drive vss is acting on'
    munge do |value|
      value.upcase
    end
  end
  newparam(:drive_id) do
    desc 'The drive ID for the volume being backed up'
  end
  newproperty(:storage_volume) do
    desc 'The drive to store the vss copies on'
    defaultto 'C'
    munge do |value|
      value.upcase
    end
  end
  newparam(:storage_id) do
    desc 'The windows drive ID for the storage drive'
  end
  newproperty(:storage_space) do
    desc 'The ammount of space allocated to storing shadow copies represented as a percent'
    defaultto '10%'
    validate do |value|
      min_size = 300 # minimum size vss supports in MB
      case value.upcase!
      when %r{^\d+$}
        if value < 0 || value > 100
          raise ArgumentError, 'value must be a percentage between 0 and 100'
        end
      when %r{^\d+B$}
        if value < (min_size * 2**20)
          raise ArgumentError, 'value must be greater than or equal to 300MB'
        end
      when %r{^\d+K$}
        if value < (min_size * 2**10)
          raise ArgumentError, 'value must be greater than or equal to 300MB'
        end
      when %r{^\d+M$}
        if value < min_size
          raise ArgumentError, 'value must be greater than or equal to 300MB'
        end
      when %r{^\d+G$}
        if value < (min_size / 2**10)
          raise ArgumentError, 'value must be greater than or equal to 300MB'
        end
      when %r{$\d+T$}
        if value < (min_size / 2**20)
          raise ArgumentError, 'value must be greater than or equal to 300MB'
        end
      when %r{$\d+P$}
        if value < (min_size / 2**30)
          raise ArgumentError, 'value must be greater than or equal to 300MB'
        end
      when %r{$\d+E$}
        if value < (min_size / 2**40)
          raise ArgumentError, 'value must be greater than or equal to 300MB'
        end
      end
    end
    munge do |value|
      if value =~ %r{$\d+$}
        value.to_s + '%'
      else
        value.upcase!
      end
    end
  end
end
